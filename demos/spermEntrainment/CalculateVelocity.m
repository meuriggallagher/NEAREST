function U = CalculateVelocity(t,Xp,sol,boundary,epsilon,domain, ...
    blockSize,NNMatrices,swimmer, procFlag)

%% Nearest-Neighbour matrix
NN = NNMatrices{1};

%% Extract points
z = deval(sol,t*2*pi);
[xForceBnd,xQuadBnd] = boundary.fn(boundary.model);
[xForce,~,xQuad] = GetSwimmerPoints(swimmer,z,t*2*pi,'cpu');

% Combine sperm and boundaries
x = MergeVectorGrids(xForce, xForceBnd);
X = MergeVectorGrids(xQuad, xQuadBnd);

%% Extract forces
NSw = length(xForce)/3;
NBnd = length(xForceBnd)/3;
N = NSw + NBnd;
nSw = numel(swimmer);

f = z(9*nSw+1:9*nSw+3*N);

%% Calculate velocity
U = EvaluateVelocityFromForce(Xp,X,x,f, ...
    epsilon,domain,blockSize,procFlag,NN);

end
