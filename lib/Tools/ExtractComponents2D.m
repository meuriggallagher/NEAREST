function [x1,x2]=ExtractComponents2D(x)

  N=length(x)/2;
  x1=x(1:N);
  x2=x(N+1:2*N);
  
end
