function [t,p] = SolveParticleTracksHydro(fourierCoefs,movingBoundary, ...
    stationaryBoundary,particleModel,nBeats,epsilon,domain, ...
    blockSize,procFlag)

[t,p] = ode45(@(T,P) EvaluateVelocityFromFourier(stationaryBoundary, ...
    movingBoundary,fourierCoefs,P,T,epsilon,domain, ...
    blockSize,procFlag),[0,nBeats],particleModel.p0(:));

end
