function [fC,stationaryBoundary,movingBoundary, ...
    epsilon,domain,blockSize] = ParLoadForces(forceFileName)

load(forceFileName,'fC','stationaryBoundary','movingBoundary', ...
    'epsilon','domain','blockSize')

end
