# NEAREST

This repository contains the code for the NEAREST project. When using this work please cite the papers it originated in:

Gallagher MT, Smith DJ. Meshfree and efficient modeling of swimming cells. Physical Review Fluids. 2018 May 31;3(5):053101.

Smith DJ. A nearest-neighbour discretisation of the regularized stokeslet boundary integral equation. Journal of Computational Physics. 2018 Apr 1;358:88-102.

Copyright (c) 2022 Meurig Thomas Gallagher
