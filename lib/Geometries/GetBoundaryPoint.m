function x1 = GetBoundaryPoint(x0,model)

if isnan(x0(1))
    % Fix y,z get x
    y = x0(:,2);
    z = x0(:,3);
    
    switch model.animal
        case 'mouse'
            x1 = model.lT * sqrt( -y.^2/model.lT^2.*(1-y/2/model.lT).^2 ...
                -z.^2/model.lB^2.*(1-y/2/model.lT).^2 + (1-y/2/model.lT).^2 );
    end
    
elseif isnan(x0(1,2))
    % Fix x,z get y
    x = x0(:,1);
    z = x0(:,3);
    
    switch model.animal
        case 'mouse'
            fun = @(y) x.^2/model.lT^2 ... 
                + y.^2/model.lT^2.*(1-y/2/model.lT).^2 ...
                + z.^2/model.lB^2.*(1-y/2/model.lT).^2 ...
                - (1-y/2/model.lT).^2;
            x1 = fsolve(fun,zeros(length(z),1));        
    end
    
elseif isnan(x0(1,3))
    % Fix x,y get z
    x = x0(:,1);
    y = x0(:,2);    
    
    switch model.animal
        case 'mouse'
            x1 =  - model.lB * sqrt( -y.^2/model.lT^2 ...
            - x.^2/model.lT^2 ./ ((1 - y/2/model.lT).^2) + 1);
        case 'rabbit'
            x1 = model.lzC * sqrt( -(x/model.lxC).^2 ...
                - (y/model.lyC).^2 + 1);
    end
else
    error('All boundary points specified')
end
