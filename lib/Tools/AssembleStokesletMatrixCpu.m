function A = AssembleStokesletMatrixCpu(xCollNodes,xQuadNodes,N,eps, ...
    domain,blockSize,NN)

procFlag = 'cpu';

M=length(xCollNodes)/3;
Q=length(xQuadNodes)/3;

% calculate number of quadrature nodes that can be used for each block
blockNodes=floor(blockSize*2^27/(9*M));

A=zeros(3*M,3*N);

for iMin=1:blockNodes:Q
    
    iMax=min(iMin+blockNodes-1,Q);
    iRange=[iMin:iMax Q+iMin:Q+iMax 2*Q+iMin:2*Q+iMax];
    
    switch domain
        case 'i'
            A=A+RegStokeslet(xCollNodes, ...
                xQuadNodes(iRange),eps,procFlag)*NN(iRange,:);
        case 'h'
            A=A+RegBlakelet(xCollNodes, ...
                xQuadNodes(iRange),eps)*NN(iRange,:);
    end
end

end
