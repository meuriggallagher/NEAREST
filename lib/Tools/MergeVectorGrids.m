function X=MergeVectorGrids(Xa,Xb)

% merges two grids made from column vectors X1, X2, 
%      assuming of the form [xcoords;ycoords;zcoords]
if isempty(Xa)
    Xa1 = [];
    Xa2 = [];
    Xa3 = [];
elseif size(Xa,2) == 1
    [Xa1, Xa2, Xa3]=ExtractComponents(Xa);
else
    Xa1 = Xa(:,1);
    Xa2 = Xa(:,2);
    Xa3 = Xa(:,3);
end

if isempty(Xb)
    Xb1 = [];
    Xb2 = [];
    Xb3 = [];
elseif size(Xb,2) == 1
    [Xb1, Xb2, Xb3]=ExtractComponents(Xb);
else
    Xb1 = Xb(:,1);
    Xb2 = Xb(:,2);
    Xb3 = Xb(:,3);
end

X=[Xa1;Xb1;Xa2;Xb2;Xa3;Xb3];
