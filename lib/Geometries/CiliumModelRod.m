%CILIUMMODELROD Generates a single rod-like cilium
%
function [x,v,RotMat] = CiliumModelRod(t,model,runType)

%-------------------------------------------------------------------
% Coarse grid - position and velocity

% Cilium radius
switch runType
    case 'hybridStokeslets'
        s = linspace(0,model.l,model.nS + 1) + 1/2/model.nS;
        s(end) = [];
    case 'nearestNeighbour'
        s = 0 : model.h_f : model.l;
        if s(end) ~= model.l
            s = [s,model.l];
        end
end

% Angle of rotation
phi = 2*pi*t + model.phaseShift; % Scaled so time in multiples of beatFreq

% Cilium position
x1 = s * sin(model.Psi) * cos(phi);
x2 = s * sin(model.Psi) * sin(phi);
x3 = s * cos(model.Psi);

% Cilium velocity
v1 = - 2*pi * s * sin(model.Psi) * sin(phi);
v2 =   2*pi * s * sin(model.Psi) * cos(phi);
v3 = 0 * v2;

% In cilium frame
x1 = -x1;
v1 = -v1;

% Rotate for posterior tilt
S = RotationMatrix(model.Th,1);

% Rotate cilium
dum = S * [x1(:)';x2(:)';x3(:)'];
x1 = dum(1,:);
x2 = dum(2,:);
x3 = dum(3,:);

% Rotate velocities
dum = S * [v1(:)';v2(:)';v3(:)'];
v1 = dum(1,:);
v2 = dum(2,:);
v3 = dum(3,:);

x = [x1(:), x2(:), x3(:)];
v = [v1(:), v2(:), v3(:)];

switch runType
    case 'hybridStokeslets'
        % Tangent, normal, binormal vectors
        tVec = [-sin(model.Psi) * cos(phi), sin(model.Psi) * sin(phi), ...
            cos(model.Psi)];
        nVec = [-cos(model.Psi) * cos(phi), cos(model.Psi) * sin(phi), ...
            -sin(model.Psi)];
        bVec = cross(tVec,nVec);
        
        % Rotate analytic rotation matrix
        dum = S * [tVec(:),nVec(:),bVec(:)];
        RotMat = dum';
        
    case 'nearestNeighbour'
        % In this case RotMat is quadrature points X
        % Rod radius
        S = 0 : model.h_q : model.l;
        if S(end) ~= model.l
            S = [S,model.l];
        end
        
        % Generate rod
        X1 = S * sin(model.Psi) * cos(phi); X1 = -X1;
        X2 = S * sin(model.Psi) * sin(phi);
        X3 = S * cos(model.Psi);
        
        % Rotate for posterior tilt
        dum = RotationMatrix(model.Th,1) * [X1(:)';X2(:)';X3(:)'];
        X1 = dum(1,:);
        X2 = dum(2,:);
        X3 = dum(3,:);
        
        RotMat = [X1(:), X2(:), X3(:)];
end
end