function p = InitialisePointInMouseNode(nP)

% Mouse parameter for node lengthscale
b = 6;
fnc = @(x,y) (x / b ./ (1 - y/2/b)).^2 + (y / b).^2 + 0.1^2 - 1;

% Initialise points
p = NaN(nP,3);
iP = 1 : nP;

while ~isempty(iP)
    keepP = 1 : length(iP);
    
    % Randomly select points in box
    px = -b + 2*b*rand(length(iP),1);
    py = -b + 2*b*rand(length(iP),1);
    
    % Exclude those outside the node
    isInNode = fnc(px,py);
    keepP(isInNode >= 0) = [];
    
    
    p(iP(keepP),1:2) = [px(keepP),py(keepP)];
    iP(keepP) = [];
end

end
