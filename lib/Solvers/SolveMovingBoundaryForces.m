function [fourierCoefs,f] = SolveMovingBoundaryForces(t,movingBoundary, ...
    statBoundary,epsilon,nFCoef,domain,nPp,blockSize,procFlag)

% Solves problem of multi swimmer trajectory and accompanying time-varying forces
% for velocity field reconstruction
%
% input:
%   x00        Initial position vector
%   b10, b20   Initial basis vectors (third basis vector is calculated via
%              cross product)
%   tRange     time range over which to calculate trajectory
%   swimmer    Structure describing how to calculate swimmer shape and
%              kinematics
%   boundary   Structure describing boundary
%   epsilon    regularisation parameter
%   domain     switch for use of image systems
%              'i' = use infinite fluid (usual)
%              'h' = use half space images (Ainley/Blakelet)
%   blockSize  memory size for stokeslet matrix block assembly in GB.
%              0.2 is a safe choice.
%   varargin   if varargin{1}='f' then includes force components
switch statBoundary.runType
    case 'hybridStokeslets'
        % Degrees of freedom
        DOF = (length(statBoundary.x)/3 ...
            + movingBoundary.model(1).nS * movingBoundary.nCilia)*3;
        
        fprintf('%i DOF\t',DOF)
        
    case 'nearestNeighbour'
        % Degrees of freedom
        DOF = size(statBoundary.NN,2);
        
        fprintf('%i DOF\t',DOF)
end

% Solve forces
f = zeros(length(t),DOF);

if nPp > 1
    parfor ii = 1:length(t)
        tic
        f(ii,:) = SolveMovingBoundaryResistanceProblem(movingBoundary, ...
            statBoundary,t(ii),epsilon,domain,blockSize,procFlag);
        dum = toc;
		fprintf('%i: %f seconds\n',ii,dum)
    end    
else
    for ii = 1:length(t)
        tic
        f(ii,:) = SolveMovingBoundaryResistanceProblem(movingBoundary, ...
            statBoundary,t(ii),epsilon,domain,blockSize,procFlag);
        dum = toc;
		fprintf('%i: %f seconds\n',ii,dum)
    end    
end

% Fourier transform
if nFCoef > 1
    fourierCoefs = CalculateFourierCoefficients(f,nFCoef);
else
    fourierCoefs = [];
end



end