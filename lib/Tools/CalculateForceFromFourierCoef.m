function f = CalculateForceFromFourierCoef(fourierCoefs,t,procFlag)

% Number of fourier coefs and force points
[nC,nF] = size(fourierCoefs.aN);

% Number of time points
nT = length(t);

% Calculate fourier series
switch procFlag
    case 'cpu'
        if nT == 1
            f = sum(fourierCoefs.aN .* exp(1i*2*pi*t'*fourierCoefs.fN),1);
            f = real(f);
        else
            f = zeros(nT,nF);
            for ii = 1 : nC
                f = f + fourierCoefs.aN(ii,:) .* exp(1i*2*pi*t'*fourierCoefs.fN(ii,:));
            end
            f = real(f);
        end
    case 'gpu'
        if nT == 1
            f = sum(fourierCoefs.aN .* exp(1i*2*pi*t'*fourierCoefs.fN),1);
            f = real(f);
        else
            f = gpuArray.zeros(nT,nF);
            for ii = 1 : nC
                f = f + fourierCoefs.aN(ii,:) .* exp(1i*2*pi*t'*fourierCoefs.fN(ii,:));
            end
            f = real(f);
        end
end

end
