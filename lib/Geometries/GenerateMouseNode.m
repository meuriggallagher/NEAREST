function [stationaryBoundary,movingBoundary] = GenerateMouseNode(h, ...
    nC,cx0,blockSize,runType)

%% Create moving boundary model
movingBoundary = struct('fn',@GenerateMovingBoundary, ...
    'model',[],'nCilia',nC,'runType',runType);

% First cilium
movingBoundary.model(1).animal = 'mouse';
movingBoundary.model(1).l = 1; % lengths scaled in multiples of cilium lengths

movingBoundary.model(1).lT = 12/2; % Mouse node x,y-plane lengthscale
movingBoundary.model(1).lB = 2.308; % Mouse node depth lengthscale
movingBoundary.model(1).lR = 1; % Mouse reichart's membrane height

movingBoundary.model(1).Psi = 30 / 180 * pi; % Semi-cone angle
movingBoundary.model(1).Th = 30 / 180 * pi; % Posterior tilt
movingBoundary.model(1).bFreq = 10; % Beat frequency Hz
movingBoundary.model(1).phaseShift = 0;

movingBoundary.model(1).nS = h(7); % Number of points along cilium

% Initialise cilia
for ii = 1 : movingBoundary.nCilia
    movingBoundary.model(ii) = movingBoundary.model(1);
    movingBoundary.model(ii).x0 = cx0(ii,:);
end


%% Create stationary boundary
stationaryBoundary = struct('fn',@ConstructMouseNode, ...
    'model',[],'x',[],'X',[],'NN',[],'runType',runType);

stationaryBoundary.model.animal = movingBoundary.model.animal;

% Node geometry parameters
stationaryBoundary.model.lT = movingBoundary.model(1).lT; % Mouse node x,y-plane lengthscale
stationaryBoundary.model.lB = movingBoundary.model(1).lB; % depth of base of node
stationaryBoundary.model.lR = movingBoundary.model(1).lR; % height of Reichert's membrane

% Main discretisation
stationaryBoundary.model.nFTop = h(1);
stationaryBoundary.model.nQTop = h(2);
stationaryBoundary.model.nFBot = h(3);
stationaryBoundary.model.nQBot = h(4);

% Patch discretisation
stationaryBoundary.model.pL = movingBoundary.model(1).l;
stationaryBoundary.model.p_f = h(5);
stationaryBoundary.model.p_q = h(6);

% Generate points
[stationaryBoundary.x,stationaryBoundary.X] = ConstructMouseNode( ...
    stationaryBoundary.model,movingBoundary);

%% Calculate Nearest-Neighbour matrix
switch runType
    case 'hybridStokeslets'
        stationaryBoundary.NN = NearestNeighbourMatrix(stationaryBoundary.X, ...
            stationaryBoundary.x,blockSize);
    case 'nearestNeighbour'
        NNstat = NearestNeighbourMatrix(stationaryBoundary.X, ...
            stationaryBoundary.x,blockSize);
        
        [~,~,~,NNmov] = movingBoundary.fn(0,movingBoundary,1);
        
        stationaryBoundary.NN = MergeNNMatrices(NNstat,NNmov);
end

end