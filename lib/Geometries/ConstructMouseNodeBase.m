function x = ConstructMouseNodeBase(model,nPts)

% Create dense array of points
NGrid = 300; %Refining this makes more quad points
[x,y,z] = meshgrid(linspace(-10,10,NGrid), ...
    linspace(-10,10,NGrid),linspace(0,4,NGrid));

% Function for calculating level set
b = model.lT;
c = model.lB;

Y = y/b;
Z = -z/c;
X = x/b./(1-y/2/b);

f = X.^2 + Y.^2 + Z.^2 - 1;

% Calculate isosurface
isoval = 0;
[~,verts,~] = isosurface(x,y,z,f,isoval,0*f);

% Reduce number of points to get desired value
minFnc = @(tol) (size(uniquetol(verts,tol,'ByRows',true),1) ...
    - nPts).^2;

reqTol = fminbnd(minFnc,0,1);

x = uniquetol(verts,reqTol,'ByRows',true);
x(:,3) = - x(:,3);
x = x(:);

end
