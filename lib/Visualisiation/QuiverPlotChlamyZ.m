
clf
pcolor(Yg,Zg,reshape((Ug.^2+Vg.^2+Wg.^2).^0.5,size(Zg)))
shading interp
colormap(parula(256))

hold on
vv = reshape(Vg,size(Xg));
ww = reshape(Wg,size(Xg));

[sx,sy] = meshgrid(-1:0.2:2,1.5);
H = streamline(Yg,Zg,vv,ww,sx,sy);
set(H,'color','white','linewidth',2)

[sx,sy] = meshgrid(-1:0.2:2,-1.5);
H = streamline(Yg,Zg,vv,ww,sx,sy);
set(H,'color','white','linewidth',2)

[sx,sy] = meshgrid(2,-1.5:0.5:1.5);
H = streamline(Yg,Zg,vv,ww,sx,sy);
set(H,'color','white','linewidth',2)
% 
% [sx,sy] = meshgrid(0:0.025:1,-0.5);
% H = streamline(Xg',Yg',uu',vv',sx,sy);
% set(H,'color','white')
% 
% [sx,sy] = meshgrid(0:0.05:1,-0.5);
% H = streamline(Xg',Yg',-uu',-vv',sx,sy);
% set(H,'color','white')
% 
% sx = [-0.2,-0.2,-0.2,-0.1];
% sy = [0.1,0.2,0.3,0.3];
% H = streamline(Xg',Yg',-uu',-vv',sx,sy);
% set(H,'color','white')
% 
% sx = [0.275];
% sy = [-0.5];
% H = streamline(Xg',Yg',-uu',-vv',sx,sy);
% set(H,'color','white')
% 
% sx = [1.1,1.1,1.1];
% sy = [-0.5,-0.45,0.1];
% H = streamline(Xg',Yg',-uu',-vv',sx,sy);
% set(H,'color','white')


dx = Yg(1:5:end,1:5:end);
dy = Zg(1:5:end,1:5:end);
du = vv(1:5:end,1:5:end);
dv = ww(1:5:end,1:5:end);
quiver(dx(:),dy(:),10*du(:),10*dv(:),0,'color',[1,1,1]);%,[0.32054,0.76027,0.5]);
% h = [1,1];