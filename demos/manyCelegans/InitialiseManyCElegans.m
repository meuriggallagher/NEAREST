function swimmer = InitialiseManyCElegans(NL,epsilon)

try
    load('manyCElegansSwimmers.mat','th0')
    
    ncElegans = numel(th0);
catch
    ncElegans = 25;
    
    % Get rotation thetas
    th0 = 2*pi*rand(ncElegans,1);
end

bodySize = [35e-3,0.25,0.3];

discrFnc = @(nL) [nL,8,4*nL,floor(4/3*8)];

discr = discrFnc(NL);

bodyFrame = cell(ncElegans,1);
for ii = 1 : ncElegans
    bodyFrame{ii} = RotationMatrix(2*pi*rand(1),3);
end

X0 = [-5,-2.5,0,2.5,5];
[X0,Y0] = meshgrid(X0,X0);

x0 = cell(ncElegans,1);
for ii = 1 : ncElegans
    x0{ii} = [X0(ii) - 0.5*cos(th0(ii)+pi/6); ...
        Y0(ii) - 0.5*sin(th0(ii)+pi/6) ; 0];
end

%% Construct swimmers
    
swimmer = SwimmersCElegans(ncElegans,@ModelCElegans2d, ...
    bodySize,discr,x0,bodyFrame,epsilon);

end
