function timeSim = TimeSimCElegans(procFlag,varargin)
% Run for various choices of discretisation
if isempty(varargin)
    NL = 8*2.^(0:11);
else
    NL = varargin{1};
end

fprintf('Running simulation with 25 C.elegans on %s\n',procFlag)

timeSim = NaN(length(NL),1);

for iN = 1 : length(NL)
    
    fprintf('\tSimulation %i of %i: N = %i\n',iN,length(NL),NL(iN))

    %% Start timer
    odeSolTimer = tic;
    
    %% Setup
    epsilon = 35e-3;
    domain='i';
    
    boundary = [];
    
    switch procFlag
        case 'cpu'
            blockSize = 4;
        case 'gpu'
            blockSize = [4,0];
    end
    
    nBeats = 3;
    tRange = [0, nBeats];
    
    %% Initialise swimmer    
    swimmer = InitialiseManyCElegans(NL(iN),epsilon);
     
    %% NN matrix
    NNMatrices = InitialiseNNMatrices('swimming',swimmer, ...
        boundary,blockSize);
    
    %% Solve
    vargsIn = {swimmer,boundary,NNMatrices};
    [t,z] = SolveTimeDependentProblem(vargsIn,tRange,epsilon, ...
        domain,blockSize,'ode45','swimming', ...
        procFlag); %#ok<ASGLU>
    
    %% Time
    timeSim(iN) = toc(odeSolTimer);

    fprintf('\tCompleted in %f seconds\n\n',timeSim(iN))
    
    switch procFlag
        case 'cpu'
            timeCElegansCPU = timeSim;
            if length(varargin) < 2
                save('cElegansSimCpu_progress.mat','NL','timeCElegansCPU')
            else
                save(varargin{2},'NL','timeCElegansCPU')
            end
            
        case 'gpu'
            timeCElegansGPU = timeSim;

            if length(varargin) < 2
                save('cElegansSimGpu_progress.mat','NL','timeCElegansGPU')
            else
                save(varargin{2},'NL','timeCElegansGPU')
            end
    end
end

switch procFlag
    case 'cpu'
        timeCElegansCPU = timeSim;
        if length(varargin) < 2
            save('cElegansSimCpu.mat','NL','timeCElegansCPU')
        else
            save(varargin{2},'NL','timeCElegansCPU')
        end
        
    case 'gpu'
        timeCElegansGPU = timeSim;

        if length(varargin) < 2
            save('cElegansSimGpu.mat','NL','timeCElegansGPU')
        else
            save(varargin{2},'NL','timeCElegansGPU')
        end
end


end
