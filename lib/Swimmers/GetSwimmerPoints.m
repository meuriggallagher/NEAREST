function [xForce,vForce,xQuad,xForceSwRotate,indSw,b1,b2] ...
    = GetSwimmerPoints(swimmer,z,t,procFlag)

nSw = length(swimmer);

x0 = cell(nSw,1);
b1 = cell(nSw,1);
b2 = cell(nSw,1);
B = cell(nSw,1);
for iSw = 1 : nSw
    % Swimmer position
    x0{iSw} = z(iSw : nSw : iSw + 2 * nSw);
    
    % Swimmer body frame
    b1{iSw} = z(3 * nSw + iSw : nSw : iSw + 5 * nSw);
    b2{iSw} = z(6 * nSw + iSw : nSw : iSw + 8 * nSw);
    b3 = cross(b1{iSw},b2{iSw});
    B{iSw} = [b1{iSw}(:) b2{iSw}(:) b3(:)];
    
end

xForceSw = cell(nSw,1); xForceSwRotate = cell(nSw,1);
vForceSw = cell(nSw,1);
xQuadSw = cell(nSw,1);
for iSw = 1 : nSw
    [xForceSw{iSw},vForceSw{iSw},xQuadSw{iSw}] = ...
        swimmer{iSw}.fn(t,swimmer{iSw}.model);
    
    xForceSwRotate{iSw} = ApplyRotationMatrix(B{iSw},xForceSw{iSw});
    xForceSw{iSw} = TranslatePoints(xForceSwRotate{iSw},x0{iSw});
    
    vForceSw{iSw} = ApplyRotationMatrix(B{iSw},vForceSw{iSw});
    
    xQuadSw{iSw} = ApplyRotationMatrix(B{iSw},xQuadSw{iSw});
    xQuadSw{iSw} = TranslatePoints(xQuadSw{iSw},x0{iSw});
end

% Get indices of swimmer points
indSw = NaN(nSw+1,1);
indSw(1) = 1;
for ii = 1 : nSw
    indSw(ii+1) = indSw(ii) + length(xForceSw{ii})/3;
end

% Merge points together
xForce = xForceSw{1};
vForce = vForceSw{1};
xQuad = xQuadSw{1};
if nSw > 1
    for iSw = 2 : nSw
        xForce = MergeVectorGrids(xForce, xForceSw{iSw});
        vForce = MergeVectorGrids(vForce, vForceSw{iSw});
        xQuad = MergeVectorGrids(xQuad, xQuadSw{iSw});
    end
end

switch procFlag
    case 'gpu'
        xForce = gpuArray(xForce);
        vForce = gpuArray(vForce);
        xQuad = gpuArray(xQuad);
end

end
