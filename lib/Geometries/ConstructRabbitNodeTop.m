function x = ConstructRabbitNodeTop(model,nPts)

lz = model.lzT - model.lzB;
yphSide = atan( (model.lyT-model.lyB)/lz );
xphSide = atan( (model.lxT-model.lxB)/lz );

%%
% Create dense array of points
NGrid = 300; %Refining this makes more quad points
[th,z] = meshgrid(linspace(0,2*pi,NGrid),linspace(0,lz,NGrid));

% Function for calculating level set
X = (z*tan(xphSide) + model.lxB) .* cos(th);
Y = (z*tan(yphSide) + model.lyB) .* sin(th);

verts = [X(:),Y(:),z(:)];

% Reduce number of points to get desired value
minFnc = @(tol) (size(uniquetol(verts,tol,'ByRows',true),1) ...
    - nPts).^2;

reqTol = fminbnd(minFnc,0,1);

x = uniquetol(verts,reqTol,'ByRows',true);

x = x(:);

end
